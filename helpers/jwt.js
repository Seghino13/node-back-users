const jwt = require('jsonwebtoken');
const signJWT = process.env.JWT_SECRET;
const generarJWT = (  data ) => {

    return new Promise( ( resolve, reject ) => {

        const payload = {
            uid: data.id,
            data: data
        };
    
        jwt.sign( payload, signJWT, {
            expiresIn: '12h'
        }, ( err, token ) => {
    
            if ( err ) {
                console.log(err);
                reject('No se pudo generar el JWT');
            } else {
                resolve( token );
            }
    
        });

    });
}

module.exports = {
    generarJWT,
}