const mongoose = require('mongoose');

//mongodb+srv://mean_user:BJVt4KfQr7dnXtk@cluster0.jrl5i.mongodb.net/demoUsers

const dbConnection = async() =>{

    try{
        await mongoose.connect( process.env.DB_CNN );
        console.log( 'mondoDB connect succesfully');
    } catch ( error ){
        console.log( error );
        throw new Error('Error en la conexion a base de datos');
    }
    
}

module.exports = {
    dbConnection
}