const { Schema, model } = require('mongoose');

const UserSchema = Schema({

    name:{
        type:String,
        required: true
    },
    email:{
        type:String,
        required: true,
        unique:true
    },
    password:{
        type:String,
        required: true
    },
    img:{
        type:String,
    },
    role:{
        type:String,
        required: true,
        default: 'USER'
    },
    google:{
        type:Boolean,
        default: false
    },
    active:{
        type: Boolean,
        default: true
    },
    created_at:{
        type: Date,
        default: new Date()
    }

});

UserSchema.method( 'toJSON', function() {
    const { __v, _id, password, ...object} = this.toObject();
    object.uuid = _id;
    return object;
});

module.exports = model( 'Users', UserSchema );