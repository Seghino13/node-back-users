/* 
    Path: /api/users
 */

const { Router } = require('express');
const { check } = require('express-validator');
const { getUsers, createUsers, updateUsers, deleteUsers } = require('../controllers/users-controller');
const { validarInputs } = require('../middlewares/validate-inputs');
const { validateJWT } =  require('../middlewares/validate-jwt');
const router = Router();


router.get( 
    '/', 
    validateJWT, 
    getUsers );

router.post( 
    '/', 
    [
        validateJWT,
        check('name', 'Name is required').not().isEmpty(),
        check('password', 'Password is required').not().isEmpty(),
        check('email', 'Email is required').isEmail(),
        validarInputs
    ],
    createUsers );

router.put( 
    '/:id', 
    [
        validateJWT,
        check('name', 'Name is required').not().isEmpty(),
        check('email', 'Email is required').isEmail(),
        check('role', 'Role is required').not().isEmpty(),
        validarInputs
    ],
    updateUsers );

router.delete( 
    '/:id',
    validateJWT,
     deleteUsers );



module.exports = router;