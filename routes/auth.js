/* 
    Path: /api/login
 */

const { Router } = require('express');
const { check } = require('express-validator');

const { login } = require('../controllers/auth-controller');

const router = Router();

router.post( 
    '/',
    [
        check('password', 'Password is required').not().isEmpty(),
        check('email', 'Email is required').isEmail(),
    ],
    login
);

module.exports = router;