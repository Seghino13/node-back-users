// Imports
require('dotenv').config();

const express = require('express');
const { dbConnection } = require('./database/config');
const cors = require('cors');


//Create serves express
const app = express();

//Configurar CORS
app.use( cors() );

//lectura y parseo del body
app.use( express.json() );

//DB
dbConnection();


//routes
app.use('/api/users', require('./routes/users-routes') );
app.use('/api/login', require('./routes/auth') );


//Server listen
app.listen( process.env.PORT , ()=>{
    console.log( 'Servidor corriendo puerto', process.env.PORT);
});