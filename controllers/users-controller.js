const { response } = require('express');
const { validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');

const Usuario = require('../models/user');

const getUsers = async ( req, res )=>{

 const users =  await Usuario.find();
    res.json({
        response: true,
        users,
        uid: req.uid
    });

}

const createUsers = async( req, res = response )=>{
    
    const {  email, password } = req.body;

    try{

        const emailExists = await Usuario.findOne({ email });

        if( emailExists ){
            res.status( 400 ).json({
                response: true,
                msg: 'Email ya se encuentra registrado'
            });
        }

        const user = new Usuario( req.body );

        //Encrypt password
        const salt = bcrypt.genSaltSync();
        user.password = bcrypt.hashSync(password, salt)

        await user.save();
        
        res.json({
            response: true,
            user
        });

    }catch (error){
        res.status( 500 ).json({
            response:false,
            msn:'Error interno'
        });
    }   
}

const updateUsers = async( req, res )=>{
    //TODO validaciones del usuario
    const  uuid = req.params.id;
    

    try{

        const existUserDB =   await Usuario.findById( uuid ); 

        if( !existUserDB ){
            return  res.status(404).json({
                response: true,
                msg:'El usuario no existe'
            });
        }
        //Update user
        const { password, google, email, ...campos} = req.body;

        if(existUserDB.email !== email ){
            const emailExists = await Usuario.findOne({ email });

            if( emailExists ){
                res.status( 400 ).json({
                    response: true,
                    msg: 'Email ya se encuentra registrado'
                });
            }
        }

        campos.email = email;

        const userUpdate = await Usuario.findByIdAndUpdate( uuid, campos , { new: true } );

        res.json({
            response: true,
            userUpdate
        });
    }catch(err){
        res.status(500).json({
            response: true,
            msg:'Error actualizando usuario'
        });
    }
}

const deleteUsers =  async( req, res )=>{

    const  uuid = req.params.id;

    const existUserDB =   await Usuario.findById( uuid );

    if( !existUserDB ){
        res.status(404).json({
            response: true,
            msg:'Usuario no existe'
        });
    }

    const userUDelete = await Usuario.findByIdAndUpdate( uuid, { active: false } , { new: true});

    res.json({
        response: true,
        userUDelete
    });
    
}

module.exports = {
    getUsers,
    createUsers,
    updateUsers,
    deleteUsers
}