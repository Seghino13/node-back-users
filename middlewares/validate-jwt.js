const jwt = require('jsonwebtoken');
const signJWT = process.env.JWT_SECRET;

const validateJWT = ( req, res, next ) =>{

    const token = req.header('authorization');


    if( !token ){
        return res.status( 401 ).json({
            msg: 'token no existe'
        });
    }

    try{

        const { uid } = jwt.verify( token, signJWT ); 
        req.uid = uid;
      
    }catch (error ){
        return res.status( 401 ).json({
            msg: 'token inválido'
        });
    }

    next();
}

module.exports = {
    validateJWT
}