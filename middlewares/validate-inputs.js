const { response } = require('express-validator');
const { validationResult } = require('express-validator');

const validarInputs = ( req, res = response, next )=>{

    const errors = validationResult( req );

    if( !errors.isEmpty() ){

        return res.status( 400 ).json({
            response: false,
            errors: errors.mapped()
        });

    }

    next();
}

module.exports = {
    validarInputs
}
